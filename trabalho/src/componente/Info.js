import React from 'react';

export default class Info extends React.Component {
    render(){
        return (
            <div>
                <div>
                    <h2>Informações: {this.props.informacoes}</h2>
                    <h3>Castelão {this.props.castelao}</h3>
                    <h3><date></date> {this.props.date}</h3>
                    <h3><time datetime="16:10">afternoon</time>{this.props.time}</h3>
                </div>
            </div>
        );
    }
    
}
