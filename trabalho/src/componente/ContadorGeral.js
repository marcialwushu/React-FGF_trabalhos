import React from 'react';
import Placar from './componente/Placar.js';
import Contador from './componente/Contador.js';
import Info from './componente/Info.js';

export default class ContadorGeral extends React.Component {
    render(){
        return (
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="single-blog-item">
                            <Placar tipo ='Mandante'
                                    tipo2 ='Ceará' />
                            <Contador />
                        </div>
                    </div>
                

                    <div class="col-md-4">
                        <div class="single-blog-item">
                            <Info />
                            
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="single-blog-item">
                            <Placar tipo='Visitante'
                                    tipo2='Fortaleza' />
                            <Contador />
                            
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}
