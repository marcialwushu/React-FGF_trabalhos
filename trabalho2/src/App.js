import React, { Component } from 'react';
import axios from 'axios';
//import pesquisa from './componentes/pesquisa.js';

export default class App extends Component{
  
  constructor(props){
    super(props);
    this.state = {
      retorno : '6065423'
    }
  }
  
  componentDidMount(event){
    axios
    .get('https://viacep.com.br/ws/'+ event)
    .then(resultado => this.setState({ retorno: resultado.data}))
    .catch(erro => console.log(erro))
  }
  
  alterarCEP(event){
    this.setState({cep : event.target.value});
    this.alterarCEP();
  }
  
  render(){
    return(
      <div>
        CEP:<input type='number' onChange={this.alteraCEP.bind(this)}/>
        <div>Logradouro:{this.state.retorno.logradouro}</div>
        <div>Bairro:{this.state.retorno.bairro}</div>
        <div>Cidade:{this.state.retorno.localidade}</div>
        <div>Estado:{this.state.retorno.uf}</div>
      </div>
      )
  }
}
  
  
  


  